using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepthNode
{
    public Vector2Int nodePos;//格子位置坐标

    public DepthNode parentNode;//父节点
    public bool isObstacle;//格子是否是障碍

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="pos">格子位置</param>
    /// <param name="type">格子类型</param>
    public DepthNode(Vector2Int pos, bool isObstacle)
    {
        nodePos = pos;
        this.isObstacle = isObstacle;
    }
}
