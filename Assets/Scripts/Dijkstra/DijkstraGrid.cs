using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DijkstraGrid : MonoBehaviour
{
    private DijkstraNode[,] nodes;//所有节点

    public Tilemap roadMap;//当前地图
    public Tilemap blockMap;//障碍物地图
    public Tilemap pathMap;//路径显示的地图
    private int width;//地图宽
    private int height;//地图高
    private Vector3 gridOriginPos;//地图起点坐标

    public Transform startTRS;//起点信息
    private Vector2Int startPosition;
    public Transform endTRS;//终点信息
    private Vector2Int endPosition;

    public TileBase findTile;//寻找过的地方显示的瓦片
    public TileBase pathTile;//最终路线显示的瓦片

    private HashSet<DijkstraNode> foundedHashSet = new HashSet<DijkstraNode>();//当前节点是否走过的列表
    private List<DijkstraNode> auxiliaryList = new List<DijkstraNode>();//需要向四周扩散的节点的列表

    private void Awake()
    {
        InitMap();
    }

    private void Start()
    {
        var pathNodes = FindPath(startPosition, endPosition);
        StartCoroutine(ShowFindArea(pathNodes));
    }

    /// <summary>
    /// 初始化地图
    /// </summary>
    public void InitMap()
    {
        //获取起点终点信息
        startPosition = new Vector2Int(Mathf.RoundToInt(startTRS.position.x), Mathf.RoundToInt(startTRS.position.y));
        endPosition = new Vector2Int(Mathf.RoundToInt(endTRS.position.x), Mathf.RoundToInt(endTRS.position.y));

        //获取当前地图信息
        width = roadMap.cellBounds.size.x;
        height = roadMap.cellBounds.size.y;
        gridOriginPos = roadMap.GetCellCenterWorld(new Vector3Int(-width / 2, -height / 2, 0));

        //初始化地图
        nodes = new DijkstraNode[width, height];
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                TileBase tile = blockMap.GetTile(new Vector3Int(x - width / 2, y - height / 2, 0));
                bool nodeIsObstacle = tile != null;
                DijkstraNode node = new DijkstraNode(new Vector2Int(x - width / 2, y - height / 2), nodeIsObstacle);
                nodes[x, y] = node;
            }
        }
    }

    public List<DijkstraNode> FindPath(Vector2Int startPos, Vector2Int endPos)
    {
        //起点终点是否在地图内
        if (startPos.x + width / 2 < 0 || startPos.x - width / 2 > 0 || startPos.y + height / 2 < 0 || startPos.y - height / 2 > 0 ||
            endPos.x + width / 2 < 0 || endPos.x - width / 2 > 0 || endPos.y + height / 2 < 0 || endPos.y - height / 2 > 0)
        {
            Debug.Log("起点或者终点在地图范围外");
            return null;
        }

        //得到起点和终点对应的节点
        DijkstraNode startNode = nodes[startPos.x + width / 2, startPos.y + height / 2];
        DijkstraNode endNode = nodes[endPos.x + width / 2, endPos.y + height / 2];

        //起点终点是否是阻挡物
        if (startNode.isObstacle || endNode.isObstacle)
        {
            Debug.Log("起点或者终点不在可行路面内");
            return null;
        }

        //设置好起点并且把起点加入寻找过的列表
        startNode.parentNode = null;
        auxiliaryList.Add(startNode);
        foundedHashSet.Add(startNode);

        while (auxiliaryList.Count > 0)
        {
            //找到准备扩散点的列表中离起点最近的那个点
            auxiliaryList.Sort(AuxiliaryListSort);

            DijkstraNode currentNode = auxiliaryList[0];//取出要扩散的那个点作为新的起点
            auxiliaryList.RemoveAt(0);//清除掉它，免得到时候找的还是它，那就死循环了

            //找周围的点
            //加斜方向莫名奇妙的就要遍历甚至比广度优先还多了 反正也挺抽象的
            //推测原因是因为多了四个方向,坐标距离相同的点就多了,明明已经找到了终点,但是排完序后会以其他相同或者更少距离的点开始寻找,走很多冤枉路
            //FindNearNodeToOpenList(currentNode.nodePos.x - 1, currentNode.nodePos.y + 1, currentNode);//左上
            FindNearNodeToOpenList(currentNode.nodePos.x, currentNode.nodePos.y + 1, currentNode);//上
            //FindNearNodeToOpenList(currentNode.nodePos.x + 1, currentNode.nodePos.y + 1, currentNode);//右上
            FindNearNodeToOpenList(currentNode.nodePos.x - 1, currentNode.nodePos.y, currentNode);//左
            FindNearNodeToOpenList(currentNode.nodePos.x + 1, currentNode.nodePos.y, currentNode);//右
            //FindNearNodeToOpenList(currentNode.nodePos.x - 1, currentNode.nodePos.y - 1, currentNode);//左下
            FindNearNodeToOpenList(currentNode.nodePos.x, currentNode.nodePos.y - 1, currentNode);//下
            //FindNearNodeToOpenList(currentNode.nodePos.x + 1, currentNode.nodePos.y - 1, currentNode);//右下

            //判断当前新的起点是不是终点
            if (currentNode == endNode)
            {
                List<DijkstraNode> pathList = new List<DijkstraNode>
                {
                    endNode
                };
                //回溯列表寻找路径
                while (endNode.parentNode != null)
                {
                    pathList.Add(endNode.parentNode);
                    endNode = endNode.parentNode;
                }
                pathList.Reverse();
                return pathList;
            }
        }

        return null;
    }

    /// <summary>
    /// 寻找周围的点并且将它放入开启列表
    /// </summary>
    /// <param name="x">x位置</param>
    /// <param name="y">y位置</param>
    /// <param name="g">斜着的传1.4 正向的传1</param>
    /// <param name="parentNode">父节点</param>
    /// <param name="endNode">终点节点</param>
    private void FindNearNodeToOpenList(int x, int y, DijkstraNode parentNode)
    {
        //传进来的x和y是否在地图范围内
        if (x < gridOriginPos.x || x > gridOriginPos.x + width || y < gridOriginPos.y || y > gridOriginPos.y + height)
        {
            return;
        }
        DijkstraNode node = nodes[x + width / 2, y + height / 2];

        //如果周围的点是空的或者是障碍或者已经寻找过了 直接返回
        if (node == null || node.isObstacle || foundedHashSet.Contains(node))
        {
            return;
        }

        //计算node中的值
        node.parentNode = parentNode;

        //如果这个节点合法，就添加进已经找过的HashSet和需要向四周扩散的节点的List
        auxiliaryList.Add(node);
        foundedHashSet.Add(node);
    }

    /// <summary>
    /// 按照DijkstraNode中的坐标来排序
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    private int AuxiliaryListSort(DijkstraNode a, DijkstraNode b)
    {
        //曼哈顿算法
        return Mathf.Abs(a.nodePos.x - b.nodePos.x) + Mathf.Abs(a.nodePos.y - b.nodePos.y);
    }

    /// <summary>
    /// 显示寻找路径
    /// </summary>
    /// <param name="pathList"></param>
    /// <returns></returns>
    private IEnumerator ShowFindArea(List<DijkstraNode> pathList)
    {
        foreach (var item in foundedHashSet)
        {
            pathMap.SetTile((Vector3Int)item.nodePos, findTile);
            yield return new WaitForSeconds(0.05f);
        }
        foreach (var item in pathList)
        {
            pathMap.SetTile((Vector3Int)item.nodePos, pathTile);
            yield return new WaitForSeconds(0.05f);
        }
    }
}
