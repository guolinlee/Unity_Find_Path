using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BreathGrid : MonoBehaviour
{
    private BreadthNode[,] nodes;//所有节点

    public Tilemap roadMap;//当前地图
    public Tilemap blockMap;//障碍物地图
    public Tilemap pathMap;//路径显示的地图
    private int width;//地图宽
    private int height;//地图高
    private Vector3 gridOriginPos;//地图起点坐标

    public Transform startTRS;//起点信息
    private Vector2Int startPosition;
    public Transform endTRS;//终点信息
    private Vector2Int endPosition;

    public TileBase findTile;//寻找过的地方显示的瓦片
    public TileBase pathTile;//最终路线显示的瓦片

    private HashSet<BreadthNode> foundedHashSet = new HashSet<BreadthNode>();//当前节点是否走过的列表
    private Queue<BreadthNode> auxiliaryQueue = new Queue<BreadthNode>();//需要向四周扩散的节点的列表

    private void Awake()
    {
        InitMap();
    }

    private void Start()
    {
        var pathNodes = FindPath(startPosition, endPosition);
        StartCoroutine(ShowFindArea(pathNodes));
    }

    /// <summary>
    /// 初始化地图
    /// </summary>
    public void InitMap()
    {
        //获取起点终点信息
        startPosition = new Vector2Int(Mathf.RoundToInt(startTRS.position.x), Mathf.RoundToInt(startTRS.position.y));
        endPosition = new Vector2Int(Mathf.RoundToInt(endTRS.position.x), Mathf.RoundToInt(endTRS.position.y));

        //获取当前地图信息
        width = roadMap.cellBounds.size.x;
        height = roadMap.cellBounds.size.y;
        gridOriginPos = roadMap.GetCellCenterWorld(new Vector3Int(-width / 2, -height / 2, 0));

        //初始化地图
        nodes = new BreadthNode[width, height];
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                TileBase tile = blockMap.GetTile(new Vector3Int(x - width / 2, y - height / 2, 0));
                bool nodeIsObstacle = tile != null;
                BreadthNode node = new BreadthNode(new Vector2Int(x - width / 2, y - height / 2), nodeIsObstacle);
                nodes[x, y] = node;
            }
        }
    }


    public List<BreadthNode> FindPath(Vector2Int startPos, Vector2Int endPos)
    {
        //起点终点是否在地图内
        if (startPos.x + width / 2 < 0 || startPos.x - width / 2 > 0 || startPos.y + height / 2 < 0 || startPos.y - height / 2 > 0 ||
            endPos.x + width / 2 < 0 || endPos.x - width / 2 > 0 || endPos.y + height / 2 < 0 || endPos.y - height / 2 > 0)
        {
            Debug.Log("起点或者终点在地图范围外");
            return null;
        }

        //得到起点和终点对应的节点
        BreadthNode startNode = nodes[startPos.x + width / 2, startPos.y + height / 2];
        BreadthNode endNode = nodes[endPos.x + width / 2, endPos.y + height / 2];

        //起点终点是否是阻挡物
        if (startNode.isObstacle || endNode.isObstacle)
        {
            Debug.Log("起点或者终点不在可行路面内");
            return null;
        }

        //设置好起点并且把起点加入寻找过的列表
        startNode.parentNode = null;
        auxiliaryQueue.Enqueue(startNode);
        foundedHashSet.Add(startNode);

        //开始扩散
        while (auxiliaryQueue.Count > 0)
        {
            BreadthNode currentNode = auxiliaryQueue.Dequeue();//取出要扩散的那个点作为新的起点

            //找周围的点
            //加了斜方向的话就太抽象了 父节点记录都是按照斜着来记录的 明明可以直线却要拐个弯 可以把注释取消了试一下
            //FindNearNodeToOpenList(currentNode.nodePos.x - 1, currentNode.nodePos.y + 1, currentNode);//左上
            FindNearNodeToOpenList(currentNode.nodePos.x, currentNode.nodePos.y + 1, currentNode);//上
            //FindNearNodeToOpenList(currentNode.nodePos.x + 1, currentNode.nodePos.y + 1, currentNode);//右上
            FindNearNodeToOpenList(currentNode.nodePos.x - 1, currentNode.nodePos.y, currentNode);//左
            FindNearNodeToOpenList(currentNode.nodePos.x + 1, currentNode.nodePos.y, currentNode);//右
            //FindNearNodeToOpenList(currentNode.nodePos.x - 1, currentNode.nodePos.y - 1, currentNode);//左下
            FindNearNodeToOpenList(currentNode.nodePos.x, currentNode.nodePos.y - 1, currentNode);//下
            //FindNearNodeToOpenList(currentNode.nodePos.x + 1, currentNode.nodePos.y - 1, currentNode);//右下

            //判断当前新的起点是不是终点
            if (currentNode == endNode)
            {
                List<BreadthNode> pathList = new List<BreadthNode>
                {
                    endNode
                };
                //回溯列表寻找路径
                while (endNode.parentNode != null)
                {
                    pathList.Add(endNode.parentNode);
                    endNode = endNode.parentNode;
                }
                pathList.Reverse();
                return pathList;
            }
        }
        return null;
    }

    private void FindNearNodeToOpenList(int x, int y, BreadthNode parentNode)
    {
        //传进来的x和y是否在地图范围内
        if (x < gridOriginPos.x || x > gridOriginPos.x + width || y < gridOriginPos.y || y > gridOriginPos.y + height)
        {
            return;
        }
        BreadthNode node = nodes[x + width / 2, y + height / 2];

        //如果周围的点是空的或者是障碍或者已经寻找过了 直接返回
        if (node == null || node.isObstacle || foundedHashSet.Contains(node))
        {
            return;
        }

        //计算node中的值
        node.parentNode = parentNode;

        //如果这个节点合法，就添加进已经找过的HashSet和需要向四周扩散的节点的queue
        foundedHashSet.Add(node);
        auxiliaryQueue.Enqueue(node);
    }

    /// <summary>
    /// 显示寻找路径
    /// </summary>
    /// <param name="pathList"></param>
    /// <returns></returns>
    private IEnumerator ShowFindArea(List<BreadthNode> pathList)
    {
        foreach (var item in foundedHashSet)
        {
            pathMap.SetTile((Vector3Int)item.nodePos, findTile);
            yield return new WaitForSeconds(0.05f);
        }
        foreach (var item in pathList)
        {
            pathMap.SetTile((Vector3Int)item.nodePos, pathTile);
            yield return new WaitForSeconds(0.05f);
        }
    }
}
