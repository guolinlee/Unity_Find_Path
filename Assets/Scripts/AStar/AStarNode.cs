using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 格子的节点信息
/// </summary>
public class AStarNode
{
    public Vector2Int nodePos;//格子位置坐标

    public float g;//离起点的距离
    public float h;//离终点的距离
    public float f => g + h;//总距离

    public AStarNode parentNode;//父节点
    public bool isObstacle;//格子是否是障碍

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="pos">格子位置</param>
    /// <param name="type">格子类型</param>
    public AStarNode(Vector2Int pos, bool isObstacle)
    {
        nodePos = pos;
        this.isObstacle = isObstacle;
    }
}
