using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.Tilemaps;
using static UnityEditor.Progress;

/// <summary>
/// A星管理者类
/// </summary>
public class AStarGrid : MonoBehaviour
{
    private AStarNode[,] nodes;//所有格子对象

    public Tilemap roadMap;//当前地图
    public Tilemap blockMap;//障碍物地图
    public Tilemap pathMap;//路径显示的地图
    private int width;//地图宽
    private int height;//地图高
    private Vector3 gridOriginPos;//地图起点坐标

    public Transform startTRS;//起点信息
    private Vector2Int startPosition;
    public Transform endTRS;//终点信息
    private Vector2Int endPosition;

    private List<AStarNode> openList = new List<AStarNode>();//开启列表
    private HashSet<AStarNode> closedList = new HashSet<AStarNode>();//关闭列表
    private List<AStarNode> showList = new List<AStarNode>();//寻过的地方的列表

    public TileBase findTile;//寻找过的地方显示的瓦片
    public TileBase pathTile;//最终路线显示的瓦片

    private void Awake()
    {
        InitMap();
    }

    private void Start()
    {
        List<AStarNode> pathNodes = FindPath(startPosition, endPosition);
        StartCoroutine(ShowFindArea(pathNodes));
    }

    /// <summary>
    /// 初始化地图
    /// </summary>
    private void InitMap()
    {
        //获取起点终点信息
        startPosition = new Vector2Int(Mathf.RoundToInt(startTRS.position.x), Mathf.RoundToInt(startTRS.position.y));
        endPosition = new Vector2Int(Mathf.RoundToInt(endTRS.position.x), Mathf.RoundToInt(endTRS.position.y));

        //获取当前地图信息
        width = roadMap.cellBounds.size.x;
        height = roadMap.cellBounds.size.y;
        gridOriginPos = roadMap.GetCellCenterWorld(new Vector3Int(-width / 2, -height / 2, 0));

        //初始化地图
        nodes = new AStarNode[width, height];
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                TileBase tile = blockMap.GetTile(new Vector3Int(x - width / 2, y - height / 2, 0));
                bool nodeIsObstacle = tile != null;
                AStarNode node = new AStarNode(new Vector2Int(x - width / 2, y - height / 2), nodeIsObstacle);
                nodes[x, y] = node;
            }
        }
    }

    /// <summary>
    /// 寻路函数
    /// </summary>
    /// <param name="startPos">当前点</param>
    /// <param name="endPos">终点</param>
    /// <returns>最终路径的list</returns>
    public List<AStarNode> FindPath(Vector2Int startPos, Vector2Int endPos)
    {
        //起点终点是否在地图内
        if (startPos.x + width / 2 < 0 || startPos.x - width / 2 > 0 || startPos.y + height / 2 < 0 || startPos.y - height / 2 > 0 ||
            endPos.x + width / 2 < 0 || endPos.x - width / 2 > 0 || endPos.y + height / 2 < 0 || endPos.y - height / 2 > 0)
        {
            Debug.Log("起点或者终点在地图范围外");
            return null;
        }

        //得到起点和终点对应的节点
        AStarNode startNode = nodes[startPos.x + width / 2, startPos.y + height / 2];
        AStarNode endNode = nodes[endPos.x + width / 2, endPos.y + height / 2];

        //起点终点是否是阻挡物
        if (startNode.isObstacle || endNode.isObstacle)
        {
            Debug.Log("起点或者终点不在可行路面内");
            return null;
        }

        //清空列表
        closedList.Clear();
        openList.Clear();

        //设置好起点并且把起点放入关闭列表
        startNode.parentNode = null;
        startNode.h = 0;
        startNode.g = 0;
        closedList.Add(startNode);

        while (true)
        {
            //找周围的点
            //有斜方向和没斜方向区别不大 也可以把斜着的注释了看一下
            FindNearNodeToOpenList(startNode.nodePos.x - 1, startNode.nodePos.y + 1, 1.4f, startNode, endNode);//左上
            FindNearNodeToOpenList(startNode.nodePos.x, startNode.nodePos.y + 1, 1f, startNode, endNode);//上
            FindNearNodeToOpenList(startNode.nodePos.x + 1, startNode.nodePos.y + 1, 1.4f, startNode, endNode);//右上
            FindNearNodeToOpenList(startNode.nodePos.x - 1, startNode.nodePos.y, 1f, startNode, endNode);//左
            FindNearNodeToOpenList(startNode.nodePos.x + 1, startNode.nodePos.y, 1f, startNode, endNode);//右
            FindNearNodeToOpenList(startNode.nodePos.x - 1, startNode.nodePos.y - 1, 1.4f, startNode, endNode);//左下
            FindNearNodeToOpenList(startNode.nodePos.x, startNode.nodePos.y - 1, 1f, startNode, endNode);//下
            FindNearNodeToOpenList(startNode.nodePos.x + 1, startNode.nodePos.y - 1, 1.4f, startNode, endNode);//右下

            //死路判断 开启列表为空都还没找到终点 那么就是死路
            if (openList.Count == 0)
            {
                Debug.Log("死路");
                return null;
            }

            //找到开启列表中f最小的那个点
            openList.Sort(OpenListSort);

            //放入关闭列表 从开启列表中移除
            closedList.Add(openList[0]);
            startNode = openList[0];//f值最小的那个点作为新的当前的起点
            openList.RemoveAt(0);

            //判断当前新的起点是不是终点
            if (startNode == endNode)
            {
                List<AStarNode> pathList = new List<AStarNode>
                {
                    endNode
                };
                //回溯关闭列表寻找路径
                while (endNode.parentNode != null)
                {
                    pathList.Add(endNode.parentNode);
                    endNode = endNode.parentNode;
                }
                pathList.Reverse();
                return pathList;
            }
        }

    }

    /// <summary>
    /// 寻找周围的点并且将它放入开启列表
    /// </summary>
    /// <param name="x">x位置</param>
    /// <param name="y">y位置</param>
    /// <param name="g">斜着的传1.4 正向的传1</param>
    /// <param name="parentNode">父节点</param>
    /// <param name="endNode">终点节点</param>
    private void FindNearNodeToOpenList(int x, int y, float g, AStarNode parentNode, AStarNode endNode)
    {
        //传进来的x和y是否在地图范围内
        if (x < gridOriginPos.x || x > gridOriginPos.x + width || y < gridOriginPos.y || y > gridOriginPos.y + height)
        {
            return;
        }
        AStarNode node = nodes[x + width / 2, y + height / 2];

        //如果周围的点是空的或者是障碍或者已经寻找过了 直接返回
        if (node == null || node.isObstacle || closedList.Contains(node) || openList.Contains(node))
        {
            return;
        }

        //计算node中的值
        node.parentNode = parentNode;
        node.g = parentNode.g + g;
        node.h = Mathf.Abs(endNode.nodePos.x - node.nodePos.x) + Mathf.Abs(endNode.nodePos.y - node.nodePos.y);

        //如果该点合法，则加入开启列表
        openList.Add(node);
        showList.Add(node);
    }

    /// <summary>
    /// 按照AStartNode中的f来排序
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    private int OpenListSort(AStarNode a, AStarNode b)
    {
        if (a.f >= b.f)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    /// <summary>
    /// 显示寻找路径
    /// </summary>
    /// <param name="pathList"></param>
    /// <returns></returns>
    private IEnumerator ShowFindArea(List<AStarNode> pathList)
    {
        foreach (var item in showList)
        {
            pathMap.SetTile((Vector3Int)item.nodePos, findTile);
            yield return new WaitForSeconds(0.05f);
        }
        foreach (var item in pathList)
        {
            pathMap.SetTile((Vector3Int)item.nodePos, pathTile);
            yield return new WaitForSeconds(0.05f);
        }
    }
}
